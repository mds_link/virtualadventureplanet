﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerConnection : NetworkBehaviour
{
    [SerializeField]
    private Camera playerCamera;
    [SerializeField]
    private GameObject playerCharacter;

    private GameObject sceneCamera;

    void Start()
    {
        if(!isLocalPlayer)
        {
            return;
        }
        SetupCameras();
        StartMyPlayerConnection();    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            playerCamera.transform.Translate(new Vector3(0, 1, 0));
        }
    }

    private void OnDestroy()
    {
        if (sceneCamera != null)
        {
            sceneCamera.SetActive(true);
        }
    }

    private void SetupCameras()
    {
        sceneCamera = GameObject.Find("Scene Camera");
        sceneCamera.SetActive(false);
        playerCamera.enabled = true;
    }

    private void StartMyPlayerConnection()
    {
        CmdSpawnPlayerCharacter();
    }

    [Command]
    void CmdSpawnPlayerCharacter()
    {
        GameObject spawnedCharacter = Instantiate(playerCharacter);
        NetworkServer.SpawnWithClientAuthority(spawnedCharacter, connectionToClient);
    }
}
