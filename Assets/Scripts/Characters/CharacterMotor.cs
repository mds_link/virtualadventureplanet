﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMotor : MonoBehaviour
{
    private Rigidbody characterRigidbody;
    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;

    private void Start()
    {
        characterRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    public void Move(Vector3 newVelocity)
    {
        velocity = newVelocity;
    }

    public void Rotate(Vector3 newRotation)
    {
        rotation = newRotation;
    }

    private void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            characterRigidbody.MovePosition(characterRigidbody.position + velocity * Time.fixedDeltaTime);
        }
    }

    private void PerformRotation()
    {
        if (rotation != Vector3.zero)
        {
            characterRigidbody.MoveRotation(characterRigidbody.rotation * Quaternion.Euler(rotation));
        }
    }
}
