﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CharacterMotor))]
public class CustomCharacterController : NetworkBehaviour
{
    [SerializeField]
    private float speed = 10f;
    [SerializeField]
    private float rotationSpeed = 4f;

    private CharacterMotor characterMotor;
    private Transform playerCameraTransform;

    void Start()
    {
        characterMotor = GetComponent<CharacterMotor>();        
    }

    public override void OnStartAuthority()
    {
        if (!hasAuthority)
        {
            return;
        }

        Transform playersCameraTransform = GetPlayersCameraTransform();
        playerCameraTransform = playersCameraTransform ?? Camera.current.transform;
    }

    void Update()
    {
        if(!hasAuthority)
        {
            return;
        }

        Vector3 velocity = CalculateVelocity();
        characterMotor.Move(velocity);

        Vector3 rotation = CalculateRotation();
        characterMotor.Rotate(rotation);
    }

    private Vector3 CalculateVelocity()
    {
        float xAxis = Input.GetAxisRaw("Vertical");
        float zAxis = Input.GetAxisRaw("Horizontal");
        Vector3 groundParallelVector = new Vector3(1f, 0f, 1f);

        Vector3 xMovement = Vector3.Scale(playerCameraTransform.transform.forward, groundParallelVector).normalized * xAxis;
        Vector3 zMovement = Vector3.Scale(playerCameraTransform.transform.right, groundParallelVector).normalized * zAxis;

        return (xMovement + zMovement).normalized * speed;
    }

    private Vector3 CalculateRotation()
    {
        float yAxis = Input.GetAxisRaw("Mouse X");

        return new Vector3(0f, yAxis, 0f) * rotationSpeed;
    }

    private Transform GetPlayersCameraTransform()
    {
        var playersConnections = GameObject.FindObjectsOfType<PlayerConnection>();
        if (playersConnections.Length == 0)
        {
            return null;
        }
        return playersConnections[0].GetComponentInChildren<Camera>().transform;
    }
}
